public class Main {
    public static void main(String[] args) {
        Wrapper<Integer> obj= new Wrapper<>();
        try {
            obj.addItem(1);
            obj.addItem(3);
            obj.addItem(null);
            obj.addItem(5);
            obj.addItem(7);
            obj.addItem(3);
        }catch (Exception | Wrapper.DuplicateException e){
            System.out.println(e);
        }
        System.out.println("======= List All From Input =======");
        for(int i=0; i<obj.size();i++){
            System.out.println(obj.getItem(i));
        }
    }
}

