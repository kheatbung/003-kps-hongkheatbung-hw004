import java.util.Arrays;
public class Wrapper<T> {
    private int i,index;
    private Object[] obj = new Object[index];
    public void addItem(T t) throws NumberFormatException, DuplicateException {
        if(t == null){
            throw new NumberFormatException("Got Null Value");
        }
        for( Object i : obj){
            if(i == t){
                throw new DuplicateException();
            }
        }
        obj = Arrays.copyOf(obj,++i);
        obj[index] = t;
        index++;
    }
    public Object getItem(int index){
        return  obj[index];
    }
    public int size() {
        return obj.length;
    }
    public static class DuplicateException extends Throwable{
        public DuplicateException() {
        }
    }
}
